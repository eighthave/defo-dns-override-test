#! /bin/bash

set -ex

# configure bind9 to forward all requests
cat << EOF > /etc/bind/named.conf.options
options {
        directory "/var/cache/bind";
        auth-nxdomain no;    # conform to RFC1035
     // listen-on-v6 { any; };
        listen-on port 53 { localhost; 192.168.0.0/24; };
        allow-query { localhost; 192.168.0.0/24; };
        forwarders { 8.8.8.8; };
        recursion yes;
        };
EOF

# setup zones
cat << EOF > /etc/bind/named.conf.local
zone "local.test" {
  type master;
  file "/etc/bind/zones/db.local.test";
} ;

zone "1.0.0.127.in-addr.arpa" {
  type master;
  file "/etc/bind/zones/db.1.0.0.127";
};
EOF

# configure zone local.test
mkdir -p /etc/bind/zones
cat << EOF > /etc/bind/zones/db.local.test
\$TTL	1
@	IN	SOA	ns1.local.test. admin.local.test. (
			     1	;<serial-number>
			     1	;<time-to-refresh>
			     1	;<time-to-retry>
			604800	;<time-to-expire>
			     1)	;<minimum-TTL>
;ListNameservers
	IN	NS	ns1.local.test.
;
ns1.local.test.	IN	A	127.0.0.1
www.local.test.	IN	CNAME   svc.localnet.test.
local.test.	IN	HTTPSSVC	0 0 svc.localnet.test.
svc.localnet.test.	IN	HTTPSSVC	1 2 svc3.localnet.test. "hq=\":8003\" esnikeys=\"...\""
svc.localnet.test.	IN	HTTPSSVC	1 3 svc2.localnet.test. "h2=\":8002\" esnikeys=\"...\""
svc2.localnet.test.	IN	A	127.0.0.1
svc2.localnet.test.	IN	AAAA	::1
svc3.localnet.test.	IN	A	127.0.0.1
svc3.localnet.test.	IN	AAAA	::1
EOF

cat << EOF > /etc/bind/zones/db.1.0.0.127
\$TTL	1
@	IN	SOA	ns1.local.test. admin.local.test. (
			     1	;<serial-number>
		     1	;<time-to-refresh>
			     1	;<time-to-retry>
			604800	;<time-to-expire>
		     1)	;<minimum-TTL>
; name servers
	IN	NS	ns1.local.test.
;
1.0.0.127	IN	PTR	local.test.
1.0.0.127	IN	PTR	www.local.test.
1.0.0.127	IN	PTR	svc.local.test.
1.0.0.127	IN	PTR	svc2.local.test.
1.0.0.127	IN	PTR	svc3.local.test.
EOF
